package core

import (
	"time"

	"github.com/mssola/user_agent"
	"gopkg.in/mgo.v2/bson"
)

type userAgentModel struct {
	Mobile   bool   `bson:"mobile" json:"mobile"`
	OS       string `bson:"os" json:"os"`
	Platform string `bson:"platform" json:"platform"`
}

func NewUserAgentModel(useragent string) *userAgentModel {
	ua := userAgentModel{}
	ua.ParseUa(useragent)
	return &ua
}

func (u *userAgentModel) ParseUa(useragent string) {
	ua := user_agent.New(useragent)
	u.OS = ua.OS()
	u.Mobile = ua.Mobile()
	u.Platform = ua.Platform()
}

type metaDataModel struct {
	IpAddress string `bson:"ipaddress" json:"ipaddress"`
	Country   string `bson:"country" json:"country"`
	City      string `bson:"city" json:"city"`
	Language  string `bson:"language" json:"language"`
}

type BannerClickModel struct {
	Id        bson.ObjectId  `bson:"_id" json:"_id"`
	Banner    bson.ObjectId  `bson:"banner" json:"banner"`
	Site      bson.ObjectId  `bson:"site" json:"site"`
	Campaign  bson.ObjectId  `bson:"campaign" json:"campaign"`
	Price     float32        `bson:"price" json:"price"`
	Currency  string         `bson:"currency" json:"currency"`
	Date      time.Time      `bson:"date" json:"date"`
	MetaData  metaDataModel  `bson:"metadata" json:"metadata"`
	UserAgent userAgentModel `bson:"useragent" json:"useragent"`
	Type      string         `bson:"type" json:"type"`
}

type SelfArticleClickModel struct {
	Id        bson.ObjectId  `bson:"_id" json:"_id"`
	Site      bson.ObjectId  `bson:"site" json:"site"`
	Date      time.Time      `bson:"date" json:"date"`
	MetaData  metaDataModel  `bson:"metadata" json:"metadata"`
	UserAgent userAgentModel `bson:"useragent" json:"useragent"`
	Type      string         `bson:"type" json:"type"`
}

type ExchangeArticleClickModel struct {
	Id        bson.ObjectId  `bson:"_id" json:"_id"`
	Site      bson.ObjectId  `bson:"site" json:"site"`
	OutSite   bson.ObjectId  `bson:"outsite" json:"outsite"`
	Date      time.Time      `bson:"date" json:"date"`
	MetaData  metaDataModel  `bson:"metadata" json:"metadata"`
	UserAgent userAgentModel `bson:"useragent" json:"useragent"`
	Type      string         `bson:"type" json:"type"`
}
