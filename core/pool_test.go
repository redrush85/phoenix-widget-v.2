package core

import (
	"testing"
	"time"
)

func BenchmarkRandomString(b *testing.B) {
	for i := 0; i < b.N; i++ {
		randomString(10)
	}
}

func TestPool(t *testing.T) {
	pool := NewPool(100, 5, time.Second*3)

	if len(pool.Items) != 100 {
		t.Error("Pool creation error. Len:", len(pool.Items))
	}

	link := pool.GetString()

	if len(link) != PoolItemSize {
		t.Error("PoolItem size isn't correct. Size:", len(link))
	}

	isValid := true

	for i := 0; i < pool.maxT-1; i++ {
		isValid = pool.HeatUp(link)
	}

	if isValid == false {
		t.Error("Max temperature reached")
	}

	time.Sleep(time.Second * 4)

	isValid = pool.HeatUp(link)

	if isValid != true {
		t.Error("HeatDown doesn't work")
	}

	isValid = pool.HeatUp(link)

	if isValid != false {
		t.Error("Max temperature didn't reach")
	}

}
