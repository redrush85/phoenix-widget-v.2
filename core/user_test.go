package core

import "testing"

func TestUserLogin(t *testing.T) {

	userArray := dataManager.Users.array
	userMap := dataManager.Users.User

	if len(userArray) <= 0 {
		t.Error("Data not loaded from Users")
	}

	if len(userMap) <= 0 {
		t.Error("Map didn't create from Users array")
	}

	userArray[0].Name = "test_name"
	email := userArray[0].Email

	if userMap[email].Name != "test_name" {
		t.Errorf("Pointers doesn't pointer on the same object. Array: %s, Map: %s", userArray[0].Name, userMap[email].Name)
	}

	if userExists := dataManager.Users.UserExists("adv@mail.com"); userExists != true {
		t.Error("Can't find user adv@mail.com")
	}

	if ok := dataManager.Users.Authorize("test@mail.com", "test"); ok != true {
		t.Error("Can't login by user test@mail.com")
	}

	if ok := dataManager.Users.Authorize("adv@mail.com", "test1"); ok != false {
		t.Error("User password checking for adv@mail.com doesn't work")
	}
}
