package core

import (
	"testing"

	"gopkg.in/mgo.v2/bson"
)

func TestCreateBannerClicks(t *testing.T) {

	c := sessionDB.C("clicks")

	var result []BannerClickModel

	err := c.Find(bson.M{"type": "banner"}).All(&result)

	if err != nil {
		t.Fatal(err)
	}

	if len(result) < 2 {
		t.Error("Not enought banner clicks stats were inserting", result)
	}
}

func TestCreateSelfArticleClicks(t *testing.T) {

	c := sessionDB.C("clicks")

	var result []SelfArticleClickModel

	err := c.Find(bson.M{"type": "self"}).All(&result)

	if err != nil {
		t.Fatal(err)
	}

	if len(result) < 2 {
		t.Error("Not enought self clicks stats were inserting", result)
	}
}

func TestCreateExchangeArticleClicks(t *testing.T) {

	c := sessionDB.C("clicks")

	var result []ExchangeArticleClickModel

	err := c.Find(bson.M{"type": "exchange"}).All(&result)

	if err != nil {
		t.Fatal(err)
	}

	if len(result) < 1 {
		t.Error("Not enought exchange clicks stats were inserting", result)
	}
}
