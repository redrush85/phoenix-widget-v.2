package core

import "github.com/gin-gonic/gin"

type ResultItem struct {
	Title    string `json:"title"`
	Image    string `json:"image"`
	Type     string `json:"type"`
	URL      string `json:"url"`
	ClickURL string `json:"clickurl"`
}

type HandlerResult struct {
	WidgetOptions string       `json:"widgetoptions"`
	Items         []ResultItem `json:"items"`
}

type BuildRequestData struct {
	Url     string
	Domain  string
	Context *gin.Context
	Manager *DataManager
}

// Function to BuildWidget. System main func :)
func BuildWidget(request BuildRequestData) HandlerResult {

	// Check and Find Site data by Domain
	site, err := request.Manager.Sites.FindByDomain(request.Domain)

	if err != nil {
		return HandlerResult{}
	}

	if site.HotCacheOptions.Enabled {
		site.HotCache.Add(request.Url)
	}

	// If Site found, generate widget
	// TODO: Add methedos to Site, Campaign Managers to generate items.
	// Used test data
	res := HandlerResult{WidgetOptions: site.Country, Items: []ResultItem{
		ResultItem{"Тестовый заголовок", "12312145.jpg", "self", "http://mignews.com", "clickurl"},
		ResultItem{"Test exchange", "3edsdf34.png", "exchange", "http://ukr.net", "clickurl"},
		ResultItem{"TOP SHOP super banner", "banner_1.jpg", "banner", "http://topshop.ua", "clickurl"},
	}}

	// Add ProtectionPool string at the end of each ResultItem.ClickURL
	for index, _ := range res.Items {
		res.Items[index].ClickURL += "." + request.Manager.ProtectionPool.GetString()
	}

	return res
}
