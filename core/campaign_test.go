package core

import "testing"

func TestLoadCampaigns(t *testing.T) {

	campaignarray := dataManager.Campaigns.array
	campaignmap := dataManager.Campaigns.Campaign

	if len(campaignarray) <= 0 {
		t.Error("Data not loaded from Campaigns")
	}

	if len(campaignmap) <= 0 {
		t.Error("Map didn't create from Campaigns array")
	}

	campaignarray[0].Amount = 123

	if campaignmap[campaignarray[0].Name].Amount != 123 {
		t.Errorf("Pointers doesn't pointer on the same object. Array: %s, Map: %d", campaignarray[0].Name, campaignmap[campaignarray[0].Name].Amount)
	}

	campaignarray[0].IsActive = true

	if campaignarray[0].IsActive != true {
		t.Error("Can't set Is_active to true")
	}

	campaignarray[0].Deactivate()

	if campaignarray[0].IsActive == true {
		t.Error("Campaign Deactivate error")
	}

	// Test banners in campaign
	banners := campaignmap["Bonprix"].Banners
	if len(banners) < 2 {
		t.Error("Not enought baners in campaing Bonprix", banners)
	}

	if _, err := campaignmap["Bonprix"].FindBannerById(banners[0].Id); err != nil {
		t.Error("FindBannerById can't find banner with Id:", banners[0].Id)
	}
}
