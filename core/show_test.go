package core

import (
	"testing"

	"gopkg.in/mgo.v2/bson"
)

func TestCreateBannerShows(t *testing.T) {

	c := sessionDB.C("shows")

	var result []bannerShowModel

	err := c.Find(bson.M{"type": "banner"}).All(&result)

	if err != nil {
		t.Fatal(err)
	}

	if len(result) < 2 {
		t.Error("Not enought banner show stats were inserting", result)
	}
}

func TestCreateSelfArticleShows(t *testing.T) {

	c := sessionDB.C("shows")

	var result []widgetShowModel

	err := c.Find(bson.M{"type": "self"}).All(&result)

	if err != nil {
		t.Fatal(err)
	}

	if len(result) < 2 {
		t.Error("Not enought self article show stats were inserting", result)
	}
}

func TestCreateExchangeArticleShows(t *testing.T) {

	c := sessionDB.C("shows")

	var result []widgetShowModel

	err := c.Find(bson.M{"type": "exchange"}).All(&result)

	if err != nil {
		t.Fatal(err)
	}

	if len(result) < 1 {
		t.Error("Not enought exchange article show stats were inserting", result)
	}
}

func TestCreateWidgetShows(t *testing.T) {

	c := sessionDB.C("shows")

	var result []widgetShowModel

	err := c.Find(bson.M{"type": "widget"}).All(&result)

	if err != nil {
		t.Fatal(err)
	}

	if len(result) < 1 {
		t.Error("Not enought widget show stats were inserting", result)
	}
}
