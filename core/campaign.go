package core

import (
	"log"
	"sync"

	"time"

	"errors"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type bannerModel struct {
	Id               bson.ObjectId    `bson:"id" json:"id"`
	Name             string           `bson:"name" json:"name"`
	Priority         int              `bson:"priority" json:"priority"`
	StartDate        time.Time        `bson:"startdate" json:"startdate"`
	EndDate          time.Time        `bson:"enddate" json:"enddate"`
	TargetingOptions TargetingOptions `bson:"targeting" json:"targeting"`
	Filters          Targeting        `bson:"-" json:"-"`
}

type campaignModel struct {
	Id         bson.ObjectId `bson:"_id" json:"_id"`
	Name       string        `bson:"name" json:"name"`
	Advertiser bson.ObjectId `bson:"advertiser" json:"-"`
	StartDate  time.Time     `bson:"startdate" json:"startdate"`
	EndDate    time.Time     `bson:"enddate" json:"enddate"`
	ClickPrice int           `bson:"clickprice" json:"clickprice"`
	Amount     float32       `bson:"amount" json:"amount"`
	Priority   int           `bson:"priority" json:"priority"`
	Type       string        `bson:"type" json:"type"`
	IsActive   bool          `bson:"isactive" json:"isactive"`
	Spent      float32       `bson:"spent" json:"spent"`
	Banners    []bannerModel `bson:"banners" json:"banners"`
}

// Load targeting filters for banner
func (b *bannerModel) LoadTargeting() {
	b.Filters = Targeting{}

	// Load Geo Targeting
	if b.TargetingOptions.Geo.IsActive {
		b.Filters = append(b.Filters,
			GeoSource{Country: b.TargetingOptions.Geo.Country, City: b.TargetingOptions.Geo.City})
	}

	// Load Time targeting
	if b.TargetingOptions.Time.IsActive {
		b.Filters = append(b.Filters,
			TimeSource{StartTime: b.TargetingOptions.Time.StartTime, EndTime: b.TargetingOptions.Time.EndTime})
	}

	// Load UserAgent targeting
	if b.TargetingOptions.UserAgent.IsActive {
		b.Filters = append(b.Filters,
			UserAgentSource{Mobile: b.TargetingOptions.UserAgent.Mobile})
	}

	// Load Retargeting targeting
	if b.TargetingOptions.Retargeting.IsActive {
		b.Filters = append(b.Filters,
			RetargetingSource{Iplist: b.TargetingOptions.Retargeting.Iplist})
	}
}

// Unused func TODO: delete this if indeed
func (c *campaignModel) Deactivate() {
	c.IsActive = false
	//TODO: deactivate in DB
	//TODO: in banners of this campaign run ClearPool func

}

// Find banner by ID and return pointer if find. This function used in internal methods with Locking
func (c *campaignModel) pFindBannerById(bannerId bson.ObjectId) *bannerModel {

	for index, value := range c.Banners {
		if value.Id == bannerId {
			return &c.Banners[index]
		}
	}
	return nil
}

// Find banner by ID and return copy of banner. Safe to use
func (c *campaignModel) FindBannerById(bannerId bson.ObjectId) (bannerModel, error) {

	for _, value := range c.Banners {
		if value.Id == bannerId {
			return value, nil
		}
	}
	return bannerModel{}, errors.New("Banner not found")
}

// Load Targeting filters to banners of campaign
func (c *campaignModel) LoadBannersTargeting() {
	for index, _ := range c.Banners {
		c.Banners[index].LoadTargeting()
	}
}

type CampaignManager struct {
	array    []campaignModel
	Campaign map[string]*campaignModel
	sync.RWMutex
	collection *mgo.Collection
}

func NewCampaignManager(collection *mgo.Collection) *CampaignManager {
	return &CampaignManager{collection: collection}
}

func (sm *CampaignManager) Load() {
	// Load campaigns
	err := sm.collection.Find(nil).All(&sm.array)
	if err != nil {
		panic(err)
	}

	log.Println("Loaded campaigns:", len(sm.array))
	sm.Campaign = make(map[string]*campaignModel, len(sm.array))

	for key, value := range sm.array {
		sm.Campaign[value.Name] = &sm.array[key]

		//Load Targeting filters to banners of campaign
		sm.Campaign[value.Name].LoadBannersTargeting()
	}

}

//TODO: create method to manage banners. FindByName, Sort, etc
