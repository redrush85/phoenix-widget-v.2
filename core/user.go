package core

import (
	"log"
	"phoenix/settings"

	"time"

	"golang.org/x/crypto/bcrypt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type userModel struct {
	Id       bson.ObjectId `bson:"_id" json:"_id"`
	Name     string        `bson:"name" json:"name"`
	Password string        `bson:"password" json:"-"`
	Notes    string        `bson:"notes" json:"notes"`
	Country  string        `bson:"country" json:"country"`
	City     string        `bson:"city" json:"city"`
	Email    string        `bson:"email" json:"email"`
	Phone    string        `bson:"string" json:"string"`
	Weight   int           `bson:"weight" json:"weight"`
	Type     string        `bson:"type" json:"type"`
	Currency string        `bson:"currency" json:"currency"`
	JoinDate time.Time     `bson:"date" json:"date"`
}

type UserManager struct {
	array      []userModel
	User       map[string]*userModel
	collection *mgo.Collection
}

func NewUserManager(collection *mgo.Collection) *UserManager {
	return &UserManager{collection: collection}
}

func (um *UserManager) Load() {
	err := um.collection.Find(nil).All(&um.array)
	if err != nil {
		panic(err)
	}

	log.Println("Loaded users:", len(um.array))
	um.User = make(map[string]*userModel, len(um.array))

	for key, value := range um.array {
		um.User[string(value.Email)] = &um.array[key]
	}
}

func UserHashPassword(password string) ([]byte, error) {
	pass := []byte(password + settings.SALT)

	// hashing the password
	hashedPassword, err := bcrypt.GenerateFromPassword(pass, 10)
	if err != nil {
		return nil, err
	}

	return hashedPassword, nil
}

func (um UserManager) UserExists(email string) bool {
	_, ok := um.User[email]
	return ok
}

func (um *UserManager) Authorize(email string, pass string) bool {
	user, ok := um.User[email]
	if ok != true {
		return false
	}

	hashedPass := []byte(pass + settings.SALT)
	err := bcrypt.CompareHashAndPassword([]byte(user.Password), hashedPass)

	if err != nil {
		return false
	}

	return true
}
