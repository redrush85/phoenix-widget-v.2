package core

import (
	"time"

	"gopkg.in/mgo.v2"
)

type DataManager struct {
	// Database pointer
	database *mgo.Database

	// Pool protection
	ProtectionPool *Pool

	// Data from DB
	Users     *UserManager
	Sites     *SiteManager
	Campaigns *CampaignManager
}

func NewDataManager(database *mgo.Database) *DataManager {
	dataManager := &DataManager{database: database}
	return dataManager
}

func (manager *DataManager) Init() {
	manager.ProtectionPool = NewPool(100, 5, time.Second*10)

	// Load Users
	manager.Users = NewUserManager(manager.database.C("users"))
	manager.Users.Load()

	// Load Sites
	manager.Sites = NewSiteManager(manager.database.C("sites"))
	manager.Sites.EnsureIndexes()
	manager.Sites.Load()

	// Load Campaigns
	manager.Campaigns = NewCampaignManager(manager.database.C("campaigns"))
	manager.Campaigns.Load()

}


