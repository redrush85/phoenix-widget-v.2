package core

import (
	"time"

	"github.com/mssola/user_agent"
)

type TargetingOptions struct {
	Geo         GeoSource         `bson:"geo" json:"geo"`
	Time        TimeSource        `bson:"time" json:"time"`
	UserAgent   UserAgentSource   `bson:"useragent" json:"useragent"`
	Retargeting RetargetingSource `bson:"retargeting" json:"retargeting"`
}

type GeoSource struct {
	Country  []string `bson:"country" json:"country"`
	City     []string `bson:"city" json:"city"`
	IsActive bool     `bson:"isactive" json:"isactive"`
}

type TimeSource struct {
	StartTime int  `bson:"starttime" json:"starttime"`
	EndTime   int  `bson:"endtime" json:"endtime"`
	IsActive  bool `bson:"isactive" json:"isactive"`
}

type UserAgentSource struct {
	Mobile   bool `bson:"mobile" json:"mobile"`
	IsActive bool `bson:"is_active" json:"is_active"`
	//TODO: add variables for OS, Browser, etc
}

type RetargetingSource struct {
	Iplist   []string `bson:"iplist" json:"iplist"`
	IsActive bool     `bson:"isactive" json:"isactive"`
}

type Checker interface {
	//TODO: change map[string]string to c.Request.Header
	Check(data map[string]string) bool
}

// Geo Filter
func (s GeoSource) Check(data map[string]string) bool {
	// Check country section

	// check if country in headers
	country, ok := data["country"]

	if ok != true {
		return false
	}

	// Compare headers and settings

	// Check Country section
	ok = false
	for _, value := range s.Country {
		if value == country {
			ok = true
			break
		}
	}

	if ok == false {
		return false
	}

	// Check city section
	// If there is no need to check city (only country), return True
	if len(s.City) == 0 {
		return true
	}

	// check if city in headers
	city, ok := data["city"]

	if ok != true {
		return false
	}

	for _, value := range s.City {
		if value == city {
			return true
		}
	}

	// Return default False value
	return false
}

// Time filter
func (s TimeSource) Check(data map[string]string) bool {
	// Check of current time in alowed time range
	currentTime := time.Now()

	if currentTime.Hour() >= s.StartTime && currentTime.Hour() < s.EndTime {
		return true
	}

	// Return default False value
	return false
}

// Retargeting filter
func (s RetargetingSource) Check(data map[string]string) bool {
	// Check if ip in headers
	ip, ok := data["ip"]
	if ok != true {
		return false
	}

	// Find user IP in retargeting IP List
	for _, value := range s.Iplist {
		if ip == value {
			return true
		}
	}

	// Return default False value
	return false
}

func (s UserAgentSource) Check(data map[string]string) bool {
	useragent, ok := data["useragent"]

	if ok != true {
		return false
	}

	ua := user_agent.New(useragent)

	if s.Mobile && ua.Mobile() {
		return true
	}

	return false
}

type Targeting []Checker

func (t Targeting) Check(data map[string]string) bool {

	for _, source := range t {
		result := source.Check(data)
		if result == false {
			return false
		}
	}

	return true
}
