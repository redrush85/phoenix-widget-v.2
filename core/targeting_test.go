package core

import "testing"

func TestTargeting(t *testing.T) {

	// Good banner
	somedata1 := make(map[string]string)
	somedata1["country"] = "Ukraine"
	somedata1["city"] = "Kyiv"
	somedata1["ip"] = "127.0.0.1"
	somedata1["useragent"] = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.97 Safari/537.11"

	// Check GEO (both)
	somedata2 := make(map[string]string)
	somedata2["country"] = "Israel"
	somedata2["city"] = "Tel Aviv"
	somedata1["ip"] = "127.0.0.1"
	somedata1["useragent"] = "Mozilla/5.0 (Linux; U; Android 1.5; de-; HTC Magic Build/PLAT-RC33) AppleWebKit/528.5+ (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1"

	// Check GEO city
	somedata3 := make(map[string]string)
	somedata3["country"] = "Ukraine"
	somedata3["city"] = ""
	somedata3["ip"] = "127.0.0.1"
	somedata3["useragent"] = "Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B367 Safari/531.21.10"

	// Check IP (retargeting)
	somedata4 := make(map[string]string)
	somedata4["country"] = "Ukraine"
	somedata4["city"] = "Kyiv"
	somedata4["ip"] = "192.168.1.1"
	somedata4["useragent"] = "Mozilla/5.0 (BlackBerry; U; BlackBerry 9800; en) AppleWebKit/534.1+ (KHTML, Like Gecko) Version/6.0.0.141 Mobile Safari/534.1+"

	// Check useragent
	somedata5 := make(map[string]string)
	somedata5["country"] = "Ukraine"
	somedata5["city"] = "Kyiv"
	somedata5["ip"] = "127.0.0.1"
	somedata5["useragent"] = "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en-US; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13"

	banner := dataManager.Campaigns.array[0].Banners[0]

	result := banner.Filters.Check(somedata1)

	if result != true {
		t.Error("1 banner check - FAIL.", result)
	}

	result = banner.Filters.Check(somedata2)
	if result != false {
		t.Error("2 banner check - FAIL.", result)
	}

	result = banner.Filters.Check(somedata3)
	if result != false {
		t.Error("3 banner check - FAIL.", result)
	}

	result = banner.Filters.Check(somedata4)
	if result != false {
		t.Error("4 banner check - FAIL.", result)
	}

	result = banner.Filters.Check(somedata5)
	if result != false {
		t.Error("5 banner check - FAIL.", result)
	}

}
