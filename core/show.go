package core

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

type bannerShowModel struct {
	Id       bson.ObjectId `bson:"_id" json:"_id"`
	Banner   bson.ObjectId `bson:"banner" json:"banner"`
	Campaign bson.ObjectId `bson:"campaign" json:"campaign"`
	Site     bson.ObjectId `bson:"site" json:"site"`
	amount   int           `bson:"amount" json:"amount"`
	Date     time.Time     `bson:"date" json:"date"`
	Type     string        `bson:"type" json:"type"`
}

type widgetShowModel struct {
	Id     bson.ObjectId `bson:"_id" json:"_id"`
	Site   bson.ObjectId `bson:"site" json:"site"`
	amount int           `bson:"amount" json:"amount"`
	Date   time.Time     `bson:"date" json:"date"`
	Type   string        `bson:"type" json:"type"`
}
