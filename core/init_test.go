package core

import (
	"phoenix/settings"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var sessionDB *mgo.Database
var dataManager *DataManager

func init() {
	session, err := mgo.Dial(settings.MONGO_SERVER)

	if err != nil {
		panic(err)
	}
	session.SetMode(mgo.Monotonic, true)

	err = session.DB(settings.TEST_MONGO_DB).DropDatabase()
	if err != nil {
		panic(err)
	}

	database := session.DB(settings.TEST_MONGO_DB)
	sessionDB = database

	// Create fixtures
	user, err := CreateUsers(database)
	if err != nil {
		panic(err)
	}

	campaign, banner, err := CreateCampaigns(database, user)
	if err != nil {
		panic(err)
	}

	site, err := CreateSites(database, user, banner)
	if err != nil {
		panic(err)
	}

	err = CreateBannerClick(database, campaign, banner, site)
	if err != nil {
		panic(err)
	}

	err = CreateSelfArticleClick(database, site)
	if err != nil {
		panic(err)
	}

	err = CreateExchangeArticleClick(database, site)
	if err != nil {
		panic(err)
	}

	err = CreateBannerShow(database, campaign, banner, site)
	if err != nil {
		panic(err)
	}

	err = CreateWidgetShow(database, site)
	if err != nil {
		panic(err)
	}

	err = CreateArticleShow(database, site)
	if err != nil {
		panic(err)
	}

	// Load Data from DB to DataManager
	dataManager = NewDataManager(database)
	dataManager.Init()
}

// create user fixtures
func CreateUsers(database *mgo.Database) (bson.ObjectId, error) {
	c := database.C("users")

	password, err := UserHashPassword("test")
	if err != nil {
		return "", err
	}

	err = c.Insert(&userModel{
		Id:       bson.NewObjectId(),
		Name:     "Test user",
		Email:    "test@mail.com",
		Password: string(password),
		Type:     "publisher",
		Notes:    "Test publisher",
		Country:  "Ukraine",
		City:     "Kyiv",
		Phone:    "123 32 65",
		Weight:   2,
		Currency: "UAH",
		JoinDate: time.Now(),
	})

	if err != nil {
		return "", err
	}

	err = c.Insert(&userModel{
		Id:       bson.NewObjectId(),
		Name:     "Test advertiser",
		Email:    "adv@mail.com",
		Password: string(password),
		Type:     "advertiser",
		Notes:    "Test advertiser",
		Country:  "USA",
		City:     "New York",
		Phone:    "543 32 615",
		Weight:   4,
		Currency: "USD",
		JoinDate: time.Now(),
	})

	if err != nil {
		return "", err
	}

	result := userModel{}

	err = c.Find(bson.M{"email": "test@mail.com"}).One(&result)

	if err != nil {
		return "", err
	}

	return result.Id, nil
}

// Create Sites fixtures in DB
func CreateSites(database *mgo.Database, userId bson.ObjectId, banner bson.ObjectId) (bson.ObjectId, error) {
	c := database.C("sites")

	result := siteModel{}

	// TODO: add default images
	err := c.Insert(&siteModel{
		Id:              bson.NewObjectId(),
		Publisher:       userId,
		Subscribers:     []bson.ObjectId{banner},
		Name:            "mignews",
		Domain:          "http://mignews.com",
		Created:         time.Now(),
		HotCacheOptions: hotCacheOptions{Enabled: true, Size: 5, Timer: time.Minute * 5},
	})

	if err != nil {
		return "", err
	}

	err = c.Insert(&siteModel{
		Id:          bson.NewObjectId(),
		Publisher:   userId,
		Name:        "nakonu",
		Subscribers: []bson.ObjectId{banner},
		Created:     time.Now(),
		Domain:      "http://nakonu.com",
	})

	if err != nil {
		return "", err
	}

	err = c.Find(bson.M{"name": "mignews"}).One(&result)

	if err != nil {
		return "", err
	}

	return result.Id, nil
}

// Create Campaigns in DB
func CreateCampaigns(database *mgo.Database, userId bson.ObjectId) (bson.ObjectId, bson.ObjectId, error) {
	c := database.C("campaigns")

	result := campaignModel{}

	to := TargetingOptions{
		Geo:         GeoSource{Country: []string{"Ukraine"}, City: []string{"Kyiv"}, IsActive: true},
		Time:        TimeSource{StartTime: 8, EndTime: 24, IsActive: true},
		UserAgent:   UserAgentSource{Mobile: true, IsActive: true},
		Retargeting: RetargetingSource{Iplist: []string{"127.0.0.1", "127.0.0.2", "127.0.0.3"}, IsActive: true}}

	to1 := TargetingOptions{
		Geo:         GeoSource{Country: []string{"Israel"}, City: []string{"Tel Aviv"}, IsActive: true},
		Time:        TimeSource{StartTime: 8, EndTime: 24, IsActive: true},
		UserAgent:   UserAgentSource{Mobile: true, IsActive: true},
		Retargeting: RetargetingSource{Iplist: []string{"127.0.0.1", "127.0.0.2", "127.0.0.3"}, IsActive: true}}

	err := c.Insert(&campaignModel{
		Id:         bson.NewObjectId(),
		Name:       "Bonprix",
		ClickPrice: 4,
		Amount:     55,
		Advertiser: userId,
		StartDate:  time.Now(),
		EndDate:    time.Now().Add(time.Hour * 24 * 2),
		Banners: []bannerModel{
			bannerModel{Id: bson.NewObjectId(), Name: "Bon banner one", StartDate: time.Now(),
				EndDate:          time.Now().Add(time.Hour * 24 * 2),
				TargetingOptions: to},
			bannerModel{Id: bson.NewObjectId(), Name: "Bon banner two", StartDate: time.Now(),
				EndDate:          time.Now().Add(time.Hour * 24 * 3),
				TargetingOptions: to1}},
	})
	if err != nil {
		return "", "", err
	}

	err = c.Insert(&campaignModel{
		Id:         bson.NewObjectId(),
		Name:       "test campaign 1",
		ClickPrice: 2,
		Amount:     100,
		Advertiser: userId,
		StartDate:  time.Now(),
		EndDate:    time.Now().Add(time.Hour * 24 * 2),
		Banners: []bannerModel{
			bannerModel{Id: bson.NewObjectId(), Name: "First Banner", StartDate: time.Now(),
				EndDate:          time.Now().Add(time.Hour * 24 * 2),
				TargetingOptions: to},
			bannerModel{Id: bson.NewObjectId(), Name: "Second Banner", StartDate: time.Now(),
				EndDate:          time.Now().Add(time.Hour * 24 * 2),
				TargetingOptions: to}},
	})

	if err != nil {
		return "", "", err
	}

	err = c.Find(bson.M{"name": "Bonprix"}).One(&result)

	if err != nil {
		return "", "", err
	}

	return result.Id, result.Banners[0].Id, nil
}

// Create Banner clicks in clicks DB
func CreateBannerClick(database *mgo.Database, campaignId, bannerId, siteId bson.ObjectId) error {
	c := database.C("clicks")

	err := c.Insert(&BannerClickModel{
		Id:       bson.NewObjectId(),
		Banner:   bannerId,
		Campaign: campaignId,
		Site:     siteId,
		Price:    1,
		Currency: "USD",
		Date:     time.Now(),
		MetaData: metaDataModel{
			IpAddress: "78.233.112.33",
			Country:   "Ukraine",
			City:      "Kyiv",
			Language:  "RU",
		},
		UserAgent: *NewUserAgentModel("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.97 Safari/537.11"),
		Type:      "banner"})

	if err != nil {
		return err
	}

	err = c.Insert(&BannerClickModel{
		Id:       bson.NewObjectId(),
		Banner:   bannerId,
		Campaign: campaignId,
		Site:     siteId,
		Price:    2,
		Currency: "UAH",
		Date:     time.Now(),
		MetaData: metaDataModel{
			IpAddress: "18.23.212.83",
			Country:   "USA",
			City:      "New York",
			Language:  "EN",
		},
		UserAgent: *NewUserAgentModel("Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B367 Safari/531.21.10"),
		Type:      "banner"})

	if err != nil {
		return err
	}
	return nil
}

// Create Self Article clicks in clicks DB
func CreateSelfArticleClick(database *mgo.Database, siteId bson.ObjectId) error {
	c := database.C("clicks")

	err := c.Insert(&SelfArticleClickModel{
		Id:   bson.NewObjectId(),
		Site: siteId,
		Date: time.Now(),
		MetaData: metaDataModel{
			IpAddress: "78.233.112.33",
			Country:   "Ukraine",
			City:      "Kyiv",
			Language:  "RU",
		},
		UserAgent: *NewUserAgentModel("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.97 Safari/537.11"),
		Type:      "self"})

	if err != nil {
		return err
	}

	err = c.Insert(&SelfArticleClickModel{
		Id:   bson.NewObjectId(),
		Site: siteId,
		Date: time.Now(),
		MetaData: metaDataModel{
			IpAddress: "18.23.212.83",
			Country:   "USA",
			City:      "New York",
			Language:  "EN",
		},
		UserAgent: *NewUserAgentModel("Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B367 Safari/531.21.10"),
		Type:      "self"})

	if err != nil {
		return err
	}
	return nil
}

// Create Exchange Article clicks in clicks DB
func CreateExchangeArticleClick(database *mgo.Database, siteId bson.ObjectId) error {
	c := database.C("clicks")

	err := c.Insert(&ExchangeArticleClickModel{
		Id:      bson.NewObjectId(),
		Site:    siteId,
		OutSite: siteId,
		Date:    time.Now(),
		MetaData: metaDataModel{
			IpAddress: "78.233.112.33",
			Country:   "Ukraine",
			City:      "Kyiv",
			Language:  "RU",
		},
		UserAgent: *NewUserAgentModel("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.97 Safari/537.11"),
		Type:      "exchange"})

	if err != nil {
		return err
	}

	return nil
}

// Create Banner shows DB stats
func CreateBannerShow(database *mgo.Database, campaignId, bannerId, siteId bson.ObjectId) error {
	c := database.C("shows")

	err := c.Insert(&bannerShowModel{
		Id:       bson.NewObjectId(),
		Banner:   bannerId,
		Campaign: campaignId,
		Site:     siteId,
		amount:   4,
		Date:     time.Now(),
		Type:     "banner",
	})

	if err != nil {
		return err
	}

	err = c.Insert(&bannerShowModel{
		Id:       bson.NewObjectId(),
		Banner:   bannerId,
		Campaign: campaignId,
		Site:     siteId,
		amount:   7,
		Date:     time.Now(),
		Type:     "banner",
	})

	if err != nil {
		return err
	}
	return nil
}

// Create widget shows DB stats
func CreateWidgetShow(database *mgo.Database, siteId bson.ObjectId) error {
	c := database.C("shows")

	err := c.Insert(&widgetShowModel{
		Id:     bson.NewObjectId(),
		Site:   siteId,
		amount: 6,
		Date:   time.Now(),
		Type:   "widget",
	})

	if err != nil {
		return err
	}

	err = c.Insert(&widgetShowModel{
		Id:     bson.NewObjectId(),
		Site:   siteId,
		amount: 9,
		Date:   time.Now(),
		Type:   "widget",
	})

	if err != nil {
		return err
	}
	return nil
}

// Create articles shows DB stats
func CreateArticleShow(database *mgo.Database, siteId bson.ObjectId) error {
	c := database.C("shows")
	// create self articles stats
	err := c.Insert(&widgetShowModel{
		Id:     bson.NewObjectId(),
		Site:   siteId,
		amount: 2,
		Date:   time.Now(),
		Type:   "self",
	})

	if err != nil {
		return err
	}

	err = c.Insert(&widgetShowModel{
		Id:     bson.NewObjectId(),
		Site:   siteId,
		amount: 5,
		Date:   time.Now(),
		Type:   "self",
	})

	if err != nil {
		return err
	}

	// create exchange articles stats
	err = c.Insert(&widgetShowModel{
		Id:     bson.NewObjectId(),
		Site:   siteId,
		amount: 9,
		Date:   time.Now(),
		Type:   "exchange",
	})

	if err != nil {
		return err
	}

	err = c.Insert(&widgetShowModel{
		Id:     bson.NewObjectId(),
		Site:   siteId,
		amount: 4,
		Date:   time.Now(),
		Type:   "exchange",
	})

	if err != nil {
		return err
	}
	return nil
}
