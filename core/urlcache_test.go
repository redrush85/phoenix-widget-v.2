package core

import (
	"testing"
	"time"
)

func TestUrlCache(t *testing.T) {

	urlcache := NewUrlCache(3, time.Second*2)

	urlcache.Add("mig1")
	urlcache.Add("mig2")
	urlcache.Add("mig3")
	urlcache.Add("mig1")
	urlcache.Add("mig2")
	urlcache.Add("mig1")
	urlcache.Add("mig3")
	urlcache.Add("mig1")
	urlcache.Add("mig2")
	urlcache.Add("mig3")
	urlcache.Add("mig4")
	urlcache.Add("mig5")
	urlcache.Add("mig5")
	urlcache.Add("mig4")
	urlcache.Add("mig4")
	urlcache.Add("mig1")
	urlcache.Add("mig3")
	urlcache.Add("mig4")

	time.Sleep(time.Second * 3)

	if len(urlcache.Urls) != 3 {
		t.Error("URlcache collector doesn't work. ", urlcache.Urls)
	}

	urlcache.StopDispatcher()

	if urlcache.IsActive {
		t.Error("URlcache Stopdispatcher doesn't work. IsActive:", urlcache.IsActive)
	}
}

func TestSiteHotCache(t *testing.T) {
	site := dataManager.Sites.Site["http://mignews.com"]

	if site.HotCache == nil {
		t.Error("HotCache doesn't load for http://mignews.com")
	}

	if !site.HotCache.IsActive {
		t.Error("HotCache doesn't active")
	}
}
