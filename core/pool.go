package core

import (
	"math/rand"
	"phoenix/timers"
	"sync"
	"time"
)

const PoolItemSize = 20
const MAX_TEMP = 5

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}

func randomString(l int) string {
	const alphanum = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	bytes := make([]byte, l)
	for i := 0; i < l; i++ {
		bytes[i] = alphanum[rand.Intn(len(alphanum))]
	}
	return string(bytes)
}

func randInt(min int, max int) int {
	return min + rand.Intn(max-min)
}

type poolItem struct {
	value       string
	temperature int
}

type Pool struct {
	size int
	maxT int
	sync.RWMutex
	Items []poolItem
}

// Constructor
func NewPool(size, maxT int, period time.Duration) *Pool {
	var items []poolItem

	for index := 0; index < size; index++ {
		items = append(items, poolItem{value: randomString(PoolItemSize)})
	}

	pool := &Pool{Items: items, size: size, maxT: maxT}
	job := timers.PeriodicTask{pool.HeadDown, period}
	job.Run()

	return pool
}

// Get random string from pool
func (p *Pool) GetString() string {
	index := rand.Intn(p.size)

	p.RLock()
	defer p.RUnlock()

	return p.Items[index].value
}

// Increase Temp of string. Return false if sting is hot ro not found
func (p *Pool) HeatUp(str string) bool {
	p.Lock()
	defer p.Unlock()

	for index := 0; index < p.size; index++ {
		if p.Items[index].value == str {
			p.Items[index].temperature += 1
			if p.Items[index].temperature >= p.maxT {
				p.Items[index].value = randomString(PoolItemSize)
				p.Items[index].temperature = 0
				return false
			}
			return true
		}
	}
	return false
}

// Decrease Temp for all items. This is periodic task
func (p *Pool) HeadDown() {
	p.Lock()
	defer p.Unlock()

	for index := 0; index < p.size; index++ {
		if p.Items[index].temperature > 0 {
			p.Items[index].temperature -= 1
		}
	}
}
