package core

import (
	"testing"

	"gopkg.in/mgo.v2/bson"
)

func TestLoadSites(t *testing.T) {

	sitearray := dataManager.Sites.array
	sitemap := dataManager.Sites.Site

	if len(sitearray) <= 0 {
		t.Error("Data not loaded from Sites")
	}

	if len(sitemap) <= 0 {
		t.Error("Map didn't create from Sites array")
	}

	sitearray[0].Name = "test_name"
	domain := sitearray[0].Domain

	if sitemap[domain].Name != "test_name" {
		t.Errorf("Pointers doesn't pointer on the same object. Array: %s, Map: %s", sitearray[0].Name, sitemap[domain].Name)
	}

	// test updating site in memory after db
	err := dataManager.Sites.collection.Update(bson.M{"_id": sitearray[0].Id}, bson.M{"$set": bson.M{"country": "Ukraine"}})
	if err != nil {
		t.Error("Could not update site in db", err)
	}

	// update in memory
	err = dataManager.Sites.MemUpsert(sitearray[0].Id)
	if err != nil {
		t.Error("Could not update site in memory", err)
	}

	if sitearray[0].Country != "Ukraine" && sitearray[0].Country != sitemap[domain].Country {
		t.Error("Site were not updated in memory")
	}

	//second test of updating site in memory (using Full update)
	sitearray[0].Country = "Israel"
	err = dataManager.Sites.collection.Update(bson.M{"_id": sitearray[0].Id}, bson.M{"$set": sitearray[0]})
	if err != nil {
		t.Error("Could not update site in db", err)
	}

	// update in memory
	err = dataManager.Sites.MemUpsert(sitearray[0].Id)
	if err != nil {
		t.Error("Could not update site in memory", err)
	}

	if sitearray[0].Country != "Israel" && sitearray[0].Country != sitemap[domain].Country {
		t.Error("Site were not updated in memory")
	}


	data := *sitemap[domain]
	data.Language = "Ukrainian"

	if sitearray[0].Language == "Ukrainian" {
		t.Error("*Pointer issue")
	}

	another_data := sitearray[0]
	another_data.Category = "News"

	if sitearray[0].Category == "News" {
		t.Error("Var = site from array issue. Need data, not pointer.")
	}

	another_data, err = dataManager.Sites.FindById(sitearray[0].Id)
	another_data.Category = "Media"

	if sitearray[0].Category == "Media" {
		t.Error("Var = site from array issue. Need data, not pointer.")
	}

	another_data, err = dataManager.Sites.FindByDomain("http://mignews.com1")

	if err == nil {
		t.Error("FindByDomain issue. Found not existing object")
	}

}
