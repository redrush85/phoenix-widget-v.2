package core

import (
	"sort"
	"sync"
	"time"
)

type url struct {
	title string `bson:"title" json:"title"`
	path  string `bson:"path" json:"path"`
	image string `bson:"image" json:"image"`
}

type UrlCache struct {
	collector map[string]int32
	Urls      []url
	IsActive  bool
	timer     time.Duration
	size      int
	sync.RWMutex
	datachan chan string
	quitchan chan bool
}

type sortStruct struct {
	key   string
	value int32
}

type sortByValue []sortStruct

func (v sortByValue) Len() int {
	return len(v)
}

func (v sortByValue) Swap(i, j int) {
	v[i], v[j] = v[j], v[i]
}

func (v sortByValue) Less(i, j int) bool {
	return v[i].value > v[j].value
}

func NewUrlCache(size int, t time.Duration) *UrlCache {
	cache := &UrlCache{collector: make(map[string]int32),
		size:     size,
		timer:    t,
		datachan: make(chan string),
		quitchan: make(chan bool),
	}

	cache.StartDispatcher()

	return cache
}

func (uc *UrlCache) StartDispatcher() {
	if uc.IsActive == true {
		return
	}

	go uc.dispatcher()
	uc.IsActive = true
}

func (uc *UrlCache) StopDispatcher() {
	if uc.IsActive == false {
		return
	}

	uc.quitchan <- true
	uc.IsActive = false
}

func (uc *UrlCache) collect() {
	var a []sortStruct

	uc.Lock()
	defer uc.Unlock()

	for key, value := range uc.collector {
		a = append(a, sortStruct{key: key, value: value})
	}

	sort.Sort(sortByValue(a))

	uc.Urls = nil

	for _, value := range a {
		//TODO: Here need to add job to parser queue
		uc.Urls = append(uc.Urls, url{path: value.key})
		if len(uc.Urls) >= uc.size {
			break
		}
	}

	uc.collector = make(map[string]int32)
}

func (uc *UrlCache) Add(url string) {
	if uc.IsActive == false {
		return
	}

	uc.datachan <- url
}

func (uc *UrlCache) dispatcher() {
	t := time.NewTicker(uc.timer)
	for {
		select {
		case newurl := <-uc.datachan:
			uc.collector[newurl] += 1

		case <-uc.quitchan:
			return

		case <-t.C:
			uc.collect()
		}
	}
}
