package core

import (
	"errors"
	"log"
	"sync"

	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Image bson.ObjectId

type widgetOptions struct {
	ItemsCount    int    `bson:"itemscount" json:"itemscount"`
	ExchangeCount int    `bson:"exchangecount" json:"exchangecount"`
	AdvCount      int    `bson:"advcount" json:"advcount"`
	Font          string `bson:"font" json:"font"`
	FontSize      int    `bson:"fontsize" json:"fontsize"`
	TitleSize     int    `bson:"titlesize" json:"titlesize"`
	Color         string `bson:"color" json:"color"`
	WidgetWidth   int    `bson:"widgetwidth" json:"widgetwidth"`
	ImageWidth    int    `bson:"imagewidth" json:"imagewidth"`
	ImageHeight   int    `bson:"imageheight" json:"imageheight"`
	ImageAlign    int    `bson:"imagealign" json:"imagealign"`
	Geom          string `bson:"geom" json:"geom"`
	WidgetGeom    string `bson:"widgetgeom" json:"widgetgeom"`
	RowCount      string `bson:"rowcount" json:"rowcount"`
}

type siteOptions struct {
	SiteBlackList     string `bson:"siteblacklist" json:"siteblacklist"`
	ArticleDeepSearch int    `bson:"articledeepsearch" json:"articledeepsearch"`
}

type hotCacheOptions struct {
	Enabled bool          `bson:"enabled" json:"enabled"`
	Size    int           `bson:"size" json:"size"`
	Timer   time.Duration `bson:"timer" json:"timer"`
}

type siteModel struct {
	Id              bson.ObjectId   `bson:"_id" json:"_id"`
	Name            string          `bson:"name" json:"name"`
	Publisher       bson.ObjectId   `bson:"publisher" json:"-"`
	Domain          string          `bson:"domain" json:"domain"`
	Category        string          `bson:"category" json:"category"`
	MetaCategory    string          `bson:"metacategory" json:"metacategory"`
	Language        string          `bson:"language" json:"language"`
	Country         string          `bson:"country" json:"country"`
	Created         time.Time       `bson:"created" json:"created"`
	Widget          widgetOptions   `bson:"widgetoptions" json:"widgetoptions"`
	Options         siteOptions     `bson:"siteoptions" json:"siteoptions"`
	DefaultImages   []Image         `bson:"defaultimages" json:"defaultimages"`
	Subscribers     []bson.ObjectId `bson:"subscribers" json:"subscribers"`
	HotCacheOptions hotCacheOptions `bson:"hotcacheoptions" json:"hotcacheoptions"`
	HotCache        *UrlCache       `bson:"-" json:"-"`
}

// Stop SiteModel services
func (sm *siteModel) ServicesStop() {
	// Stop HotCache
	if sm.HotCache != nil {
		//log.Println("Stop Hotcache for site ", sm.Domain)
		sm.HotCache.StopDispatcher()
	}
}

// Start SiteModel services
func (sm *siteModel) ServicesStart() {
	// Start HotCache
	if sm.HotCacheOptions.Enabled {
		//log.Println("Start Hotcache for site ", sm.Domain)
		sm.HotCache = NewUrlCache(sm.HotCacheOptions.Size, sm.HotCacheOptions.Timer)
	}
}

// TODO: make array and map vars private (invisible for others)
type SiteManager struct {
	array []siteModel
	Site  map[string]*siteModel
	sync.RWMutex
	collection *mgo.Collection
}

func NewSiteManager(collection *mgo.Collection) *SiteManager {
	return &SiteManager{collection: collection}
}

func (sm *SiteManager) EnsureIndexes() {

	i := mgo.Index{
		Key:        []string{"domain"},
		Unique:     true,
		DropDups:   false,
		Background: true,
		Sparse:     false,
	}

	sm.collection.EnsureIndex(i)
}

func (sm *SiteManager) Load() {
	err := sm.collection.Find(nil).All(&sm.array)
	if err != nil {
		panic(err)
	}

	log.Println("Loaded sites:", len(sm.array))
	sm.Site = make(map[string]*siteModel, len(sm.array))

	for key, value := range sm.array {
		sm.Site[value.Domain] = &sm.array[key]
		sm.Site[value.Domain].ServicesStart()
	}
}

// Find site by ID and return copy of site. Safe to use
func (sm *SiteManager) FindById(siteId bson.ObjectId) (siteModel, error) {

	// Lock manager before searching data
	sm.RLock()
	defer sm.RUnlock()

	for _, site := range sm.array {
		if siteId == site.Id {
			return site, nil
		}
	}

	return siteModel{}, errors.New("Site not found")
}

// Find site by Domain and return copy of site. Safe to use
func (sm *SiteManager) FindByDomain(domain string) (siteModel, error) {

	if domain == "" {
		return siteModel{}, errors.New("Site not found")
	}

	// Lock manager before searching data
	sm.RLock()
	defer sm.RUnlock()

	site, ok := sm.Site[domain]
	if !ok {
		return siteModel{}, errors.New("Site not found")
	}

	return *site, nil
}

// Function to update value in memory or create new one from DB
func (sm *SiteManager) MemUpsert(siteId bson.ObjectId) error {

	result := siteModel{}
	// Find Site in DB
	err := sm.collection.Find(bson.M{"_id": siteId}).One(&result)
	if err != nil {
		return err
	}

	// Lock Manager before any update operations in memory
	sm.Lock()
	defer sm.Unlock()

	for index, site := range sm.array {
		if siteId == site.Id {
			sm.array[index].ServicesStop()
			sm.array[index] = result
			sm.array[index].ServicesStart()
			return nil
		}
	}

	// Add data to memory (array, map)
	sm.array = append(sm.array, result)
	sm.Site[result.Domain] = &sm.array[len(sm.array)-1]
	sm.Site[result.Domain].ServicesStart()
	return nil
}
