package timers

import (
	"time"
)

// Struct for periodical running function with timer
type PeriodicTask struct {
	Task  func()
	Timer time.Duration
}

// Run function in goroutine periodically with timer
func (pt *PeriodicTask) Run() {
	go func() {
		t := time.NewTicker(pt.Timer)

		for {
			<-t.C
			pt.Task()
		}
	}()
}
