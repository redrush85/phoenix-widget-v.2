package webserver

import (
	"phoenix/settings"

	"github.com/gin-gonic/gin"
)

type WebServer struct {
	host        string
	staticDir   string
	templateDir string
	engine      *gin.Engine
}

func NewWebServer(host, staticDir, templateDir string) *WebServer {

	if settings.DEBUG != true {
		gin.SetMode(gin.ReleaseMode)
	}

	server := gin.Default()

	return &WebServer{host: host, staticDir: staticDir, templateDir: templateDir, engine: server}
}

func (server *WebServer) LoadMiddleware() {
	//server.engine.Use(AuthorizeMiddleware())
}

func (server *WebServer) Run() {

	// Run dipatcher
	JobQueue = make(chan Job, settings.MaxQueue)
	dispatcher := NewDispatcher(settings.MaxWorker)
	dispatcher.Run()

	server.engine.Static("/static", server.staticDir)
	server.engine.LoadHTMLGlob(server.templateDir)
	server.engine.Run(server.host)
}
