package webserver

import (
	"phoenix/core"
	"phoenix/settings"

	"gopkg.in/mgo.v2"
)

func (server *WebServer) BuildURL(database *mgo.Database, data *core.DataManager) {

	handlers := handler{database, data}

	api := server.engine.Group("/api/v1")
	{
		api.GET("/getdata", handlers.IndexJSON())
		api.GET("/getcampaigns", handlers.CampaignsJSON())
		api.GET("/headers", handlers.HeadersHandler())
		api.POST("/login", handlers.LoginHandler())
		api.GET("/sites/:siteId/image", handlers.GetSiteImagesHandler())
		api.POST("/sites/:siteId/image", handlers.UploadSiteImageHandler())
	}

	private := server.engine.Group("/api/v1/private")
	private.Use(Auth(settings.SECRET_KEY))

	private.GET("/test", handlers.TestLoginHandler())

}
