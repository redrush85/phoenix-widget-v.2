package webserver

import (
	"encoding/json"
	"io"
	"net/http"
	"strings"
	"time"

	"phoenix/core"
	"phoenix/settings"

	"log"

	jwt_lib "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type handler struct {
	db   *mgo.Database
	data *core.DataManager
}

func (h handler) IndexJSON() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Check Incoming Data
		// Check URL
		url := c.Request.URL.Query().Get("url")
		if url == "" || strings.Contains(url, "file:///") {
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}

		// Check Domain
		domain := c.Request.URL.Query().Get("domain")
		if domain == "" {
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}

		// Build Widget
		request := core.BuildRequestData{Domain: domain, Url: url, Context: c, Manager: h.data}

		job := Job{Request: request, Result: make(chan core.HandlerResult)}
		JobQueue <- job

		result := <-job.Result

		data, err := json.Marshal(result)
		if err != nil {
			c.AbortWithStatus(http.StatusInternalServerError)
		}
		c.Data(200, "application/json", data)
	}
}

func (h handler) CampaignsJSON() gin.HandlerFunc {
	return func(c *gin.Context) {
		data, err := json.Marshal(h.data.Campaigns.Campaign)
		if err != nil {
			c.AbortWithStatus(http.StatusInternalServerError)
		}
		c.Data(200, "application/json", data)
	}
}

// Show Request Headers in JSON
func (h handler) HeadersHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		data, err := json.Marshal(c.Request.Header)
		if err != nil {
			c.AbortWithStatus(http.StatusInternalServerError)
		}
		c.Data(200, "application/json", data)
	}
}

// user Login handler. Return: JWT Token
func (h handler) LoginHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		var authData LoginJSON
		c.Bind(&authData)
		log.Println(authData.User, authData.Password)

		if authData.User != "alex" || authData.Password != "a1s2d3" {
			c.JSON(401, gin.H{"error": "User with password not found"})
			return
		}

		// Create the token
		token := jwt_lib.New(jwt_lib.GetSigningMethod("HS256"))
		// Set some claims
		token.Claims["id"] = "1"
		token.Claims["user"] = "Alex"
		token.Claims["exp"] = time.Now().Add(time.Hour * 1).Unix()
		// Sign and get the complete encoded token as a string
		tokenString, err := token.SignedString([]byte(settings.SECRET_KEY))
		if err != nil {
			c.JSON(500, gin.H{"message": "Could not generate token"})
		}
		c.JSON(200, gin.H{"token": tokenString})
	}
}

func (h handler) TestLoginHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		data, ok := c.Get("token")

		if ok != true {
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}

		token := data.(*jwt_lib.Token)
		c.JSON(200, gin.H{"test data": "ok", "user": token.Claims["user"]})
	}
}

func (h handler) GetSiteImagesHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		site, err := h.data.Sites.FindById(bson.ObjectIdHex(c.Params.ByName("siteId")))
		if err != nil {
			c.JSON(404, gin.H{"error": "Site not found"})
			return
		}

		//TODO: return image
		data, err := json.Marshal(site.DefaultImages)
		if err != nil {
			c.AbortWithStatus(http.StatusInternalServerError)
		}

		c.Data(200, "application/json", data)
	}
}

func (h handler) UploadSiteImageHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		site, err := h.data.Sites.FindById(bson.ObjectIdHex(c.Params.ByName("siteId")))
		if err != nil {
			c.JSON(404, gin.H{"error": "Site not found"})
			return
		}

		// get the multipart reader for the request.
		reader, err := c.Request.MultipartReader()

		if err != nil {
			c.AbortWithStatus(http.StatusInternalServerError)
		}

		// copy each part to gridFS.
		for {
			part, err := reader.NextPart()
			if err == io.EOF {
				break
			}

			// if filename is empty, skip this iteration.
			if part.FileName() == "" {
				continue
			}

			// TODO: insert only unique images
			file, err := h.db.GridFS("fs").Create(part.FileName())
			if err != nil {
				c.AbortWithStatus(http.StatusInternalServerError)
			}

			_, err = io.Copy(file, part)
			if err != nil {
				c.AbortWithStatus(http.StatusInternalServerError)
			}

			err = file.Close()
			if err != nil {
				c.AbortWithStatus(http.StatusInternalServerError)
			}

			// update site with images ids
			// TODO: use bulk update or smth
			err = h.db.C("sites").Update(
				bson.M{"_id": site.Id},
				bson.M{"$addToSet": bson.M{"defaultimages": file.Id().(bson.ObjectId)}},
			)
			if err != nil {
				c.AbortWithStatus(http.StatusInternalServerError)
			}

			err = h.data.Sites.MemUpsert(site.Id)
			if err != nil {
				c.AbortWithStatus(http.StatusInternalServerError)
			}
		}

		c.JSON(200, gin.H{"message": "Successfully upload all images"})

	}
}
