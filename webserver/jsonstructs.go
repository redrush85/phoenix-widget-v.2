package webserver

// Login JSON
type LoginJSON struct {
	User     string `json:"user" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type User struct {
	id   string `json:"id"`
	user string `json:"user"`
}
