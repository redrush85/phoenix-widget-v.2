package settings

const DEBUG = true

const TEMPLATE_DIR = "webserver/web/templates/*"
const STATIC_DIR = "webserver/web/static"
const HTTP_HOST = "127.0.0.1:8000"
const MONGO_SERVER = "localhost"
const MONGO_DB = "phoenix_test"

const TEST_MONGO_DB = "phoenix_test"

const PARSER_IMAGE_DIR = "images/"
const PARSER_WORKERS = 4

// amount of site pages that beeing loaded in to memory
const SITEPAGE_LIMIT = 400

const SECRET_KEY = "9JVL4BCUmZRygb"

const SALT = "j|Ag0~Ty4x3=bj~=j7BrX +4UGMR~lRSHn3-WCPU^X7%RYQk7eBpC0P~bVd"

var (
	MaxWorker = 8
	MaxQueue  = 8 * 4
)
