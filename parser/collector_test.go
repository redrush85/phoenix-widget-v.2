package parser

import (
	"testing"
	"time"
)

func TestParsePage(t *testing.T) {
	StartDispatcher(5)
	ParsePage("div.textnews h1", "div.textnews img", "http://mignews.com", "http://mignews.com/news/lifestyle/world/080515_110330_89388.html")
	time.Sleep(time.Second * 10)
	t.Log("Done")
}
