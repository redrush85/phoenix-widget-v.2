package parser

import (
	"errors"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"phoenix/settings"
	"strconv"
	"strings"
)

type Image struct {
	url      string
	filename string
}

func (image *Image) Download() (string, error) {
	log.Printf("Downloading file from %s\n", image.url)

	fileURL, err := url.Parse(image.url)

	if err != nil {
		return "", err
	}

	path := fileURL.Path
	segments := strings.Split(path, "/")
	fileName := segments[len(segments)-1] // change the number to accommodate changes to the url.Path position
	fileName = checkFileName(fileName)

	file, err := os.Create(fileName)

	if err != nil {
		return "", err
	}

	defer file.Close()

	check := http.Client{
		CheckRedirect: func(r *http.Request, via []*http.Request) error {
			r.URL.Opaque = r.URL.Path
			return nil
		},
	}

	resp, err := check.Get(image.url) // add a filter to check redirect

	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	log.Println(resp.Status)

	size, err := io.Copy(file, resp.Body)

	if err != nil {
		return "", err
	}

	log.Printf("%s with %v bytes downloaded\n", fileName, size)
	image.filename = fileName
	return fileName, nil
}

func checkFileName(filename string) string {

	ext := filepath.Ext(filename)
	name := strings.TrimSuffix(filename, ext)

	newfilename := name
	count := 0

	for {
		count++
		_, err := os.Stat(settings.PARSER_IMAGE_DIR + newfilename + ext)

		if err != nil {
			return settings.PARSER_IMAGE_DIR + newfilename + ext

		} else {
			newfilename = name + "_" + strconv.Itoa(count)
		}
	}
}

func (image *Image) ValidateImageType() error {

	// open the uploaded file
	file, err := os.Open(image.filename)
	defer file.Close()

	if err != nil {
		return err
	}

	buff := make([]byte, 512) // why 512 bytes ? see http://golang.org/pkg/net/http/#DetectContentType
	_, err = file.Read(buff)

	if err != nil {
		return err
	}

	filetype := http.DetectContentType(buff)

	switch filetype {
	case "image/jpeg", "image/jpg":
		return nil

	case "image/gif":
		return nil

	case "image/png":
		return nil

	default:
		return errors.New("Invalid image type: " + filetype)
	}
}

func (image *Image) Delete() {
	err := os.Remove(image.filename)

	if err != nil {
		log.Println(err)
	}
}
