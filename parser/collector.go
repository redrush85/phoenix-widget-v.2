package parser

// A buffered channel that we can send work requests on.
var WorkQueue = make(chan WorkRequest, 100)

func ParsePage(titleMask, imageMask, imagePrefix, url string) {
	work := WorkRequest{titleMask: titleMask, imageMask: imageMask, url: url, imagePrefix: imagePrefix}
	// Push the work onto the queue.
	WorkQueue <- work
}
