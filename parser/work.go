package parser

import (
	"log"

	"github.com/PuerkitoBio/goquery"
)

type WorkRequest struct {
	imageMask   string
	titleMask   string
	url         string
	imagePrefix string
}

func (w *WorkRequest) Parse() (string, string, error) {
	doc, err := goquery.NewDocument(w.url)

	if err != nil {
		return "", "", err
	}

	title := doc.Find(w.titleMask).Text()
	image, _ := doc.Find(w.imageMask).Attr("src")

	if image != "" && w.imagePrefix != "" {
		image = w.imagePrefix + image
	}

	return title, image, nil
}

func (w *WorkRequest) ParseProcess(title, imageURL string, err error, workerId int) {
	if err != nil {
		// Handle error
		log.Println(err)
		return
	}

	//Do Something with received data
	log.Printf("Worker%d: title: %s\timage: %s\n", workerId, title, imageURL)

	// If image not parsed
	if imageURL == "" {
		//TODO: return default image
		return
	}

	image := Image{url: imageURL}
	_, err = image.Download()

	if err != nil {
		//TODO: return default image
		log.Println(err)
		return
	}

	if err = image.ValidateImageType(); err != nil {
		//TODO: return default image
		log.Println(err)
		image.Delete()
		return
	}

}
