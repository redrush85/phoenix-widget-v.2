package mongo

import (
	"gopkg.in/mgo.v2"
)

func Run(connectionUrl string) *mgo.Session {
	session, err := mgo.Dial(connectionUrl)
	if err != nil {
		panic(err)
	}

	session.SetMode(mgo.Monotonic, true)

	return session
}
