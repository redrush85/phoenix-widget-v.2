package main

import (
	"log"
	"phoenix/core"
	"phoenix/mongo"
	"phoenix/settings"
	"phoenix/webserver"
	"runtime"

	"github.com/karlseguin/ccache"
)

//GLOBAL CACHE
var Cache = ccache.New(ccache.Configure())

func main() {

	// Use all CPU cores in goroutines.
	// Attention: this parameter can slow down whole program
	runtime.GOMAXPROCS(runtime.NumCPU())

	session := mongo.Run(settings.MONGO_SERVER)
	defer session.Close()

	serverData := core.NewDataManager(session.DB(settings.MONGO_DB))
	serverData.Init()

	// Start the dispatcher.
	// log.Println("Starting the dispatcher")
	// parser.StartDispatcher(settings.PARSER_WORKERS)

	log.Println("Starting HTTP server...")
	server := webserver.NewWebServer(settings.HTTP_HOST, settings.STATIC_DIR, settings.TEMPLATE_DIR)
	server.LoadMiddleware()

	server.BuildURL(session.DB(settings.MONGO_DB), serverData)
	server.Run()
}
