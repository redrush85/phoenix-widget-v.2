package main

import (
	"testing"
	"time"

	"github.com/karlseguin/ccache"
)

func TestCache(t *testing.T) {
	cache := ccache.New(ccache.Configure())

	cache.Set("test", "test_data", time.Second)

	data := cache.Get("test")

	if data == nil {
		t.Error("Cache get fail")
	}

	if data.Value() != "test_data" {
		t.Error("Cache.Get value doesn't match")
	}

	time.Sleep(time.Second * 2)

	if data.Expired() != true {
		t.Error("TTL fail. Expires:", data.Expired())
	}

	cache.Set("test2", 123, time.Second*5)
	cache.Delete("test2")

	data = cache.Get("test2")
	if data != nil {
		t.Error("Cache delete key error. Key:", data)
	}

}
